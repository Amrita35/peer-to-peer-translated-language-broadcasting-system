# Peer to Peer Translated Language Broadcasting System

Contains conditional P2P (Peer to Peer) data transmission system, star network topology in case of Server-client connection, full duplex communication mode, dynamic sentence mapping.